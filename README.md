
# The Ideal Database Client (for JavaScript)

The number one reason I choose a database over another is the client.
Currently, I'm leaning towards RethinkDB just because it supports promises out of the box.
However, the authors don't seem to understand the node ecosystem very well and thus do not have a great JavaScript driver overall.

With my frustration, I've created [mongodb-next](https://github.com/mongodb-utils/mongodb-next) in hopes of creating a better client.
Unfortunately, this has only made me sadder once I realized the official driver probably won't ever look this good.
Instead, I'd like to share thoughts on how I think JavaScript database drivers should look and hope that databases listen.

## Await the future

Every method should return a promise.
There should be no synchronous method of anything.
The primary reason is async/await.
Make your driver future proof!

An easy way to make anything a promise is to create a `.then()` function on your prototype.
Lazily create the underlying promise and proxy `.then()` through it.

```js
Query.prototype.then = function (resolve, reject) {
  var promise = this._promise || this.run();
  return promise.then(resolve, reject);
}

Query.prototype.catch = function (reject) {
  return this.then(null, reject);
}
```

This allows queries such as:

```js
async function () {
  var user = await client.table('users').get('1234');
}
```

## Client connections

Clients __should not__ be created asynchronously.
This makes it difficult to pass around the app at start up.
Instead, they should be initialized synchronously,
but connect asynchronously, as a promise.

```js
function Client(options) {

}

Client.prototype.connect = function () {
  if (this.connected) return Promise.resolve();
  return this._connect();
}
```

This allows users to write code like:

```js
var client = new Client('tcp://127.0.0.1:6789');

app.get('/', async function () {
  await client.connect(); // always wait until the client is connected
  var user = await client.table('users').get(req.session.userid);
  // ...
})
```

Alternatively, the app could start only after all database connections have started.

```js
client.connect().then(function () {
  app.listen(3000);
});
```

In addition, each client should handle transparently:

- Autoreconnects
- Pooling

The defaults should make sense and the developer should not have to change settings in a production app for most use cases.

## Cursors

(Public) cursors should __always__ be readable streams.
Ideally, you would use [readable-stream](https://github.com/iojs/readable-stream) for maximum server-side JS compatibility.
However, in the future, ES7 streams may be used, which are not backwards compatible.

Now, I say public because, internally, cursors are just `.next()` and `.close()`.
Any public API is just syntax suger on top of that.

At a minimum, a public cursor should have:

- `.pipe()` for piping streams
- `.toArray()` to return the entire result as an array
- `.then()` as an alias for `.toArray()`

This covers 99% of the use cases for streams.
`.each()` or `.forEach()` should be replaced by the streaming API.
Here's a minimal implementation of a public cursor:

```js
var Readable = require('readable-stream/readable');
var inherits = require('util').inherits;

inherits(Cursor, Readable);

function Cursor(options) {
  options.readableObjectMode = true;
  Readable.call(this, options);
  // create the internal cursor
  this._cursor = new Client.Cursor(options);
}

// a module just for .toArray()
Cursor.prototype.toArray = require('stream-to-array');

// create promise methods
Cursor.prototype.then = function (resolve, reject) {
  return this.toArray().then(resolve, reject);
}

// implement the readable interface
Cursor.prototype._read = function () {
  if (this._cursor.closed || this.closed) return this.push(null);
  var self = this;
  this._cursor.next(function (err, result) {
    if (err) return self.emit('error', err);
    if (!result) return self.push(null);
    self.push(result);
  });
}

// implement closing the stream
Cursor.prototype.destroy = function () {
  if (this._cursor.closed) return this;
  this._cursor.close();
  this.closed = true;
  this.emit('close');
  return this;
}
```

Usage examples:

```js
// returning the entire result
var arr = await cursor.toArray();
var arr = await cursor;

// piping the result
cursor.pipe(JSONStream.stringify()).pipe(res);

// handling the results one by one
cursor.on('data', function (result) {
  console.log(result);
});

cursor.on('end', function () {
  console.log('done!');
});
```

## Updates

## Batching
